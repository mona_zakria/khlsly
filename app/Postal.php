<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postal extends Eloquent {

	use SoftDeletes;
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
