<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Upload extends Eloquent {

	use SoftDeletes;
	
	public function order()
    {
        return $this->belongsTo('App\Order');
    }

	public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }
}
