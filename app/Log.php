<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Eloquent {

	use SoftDeletes;
	
	public function order()
    {
        return $this->belongsTo('App\Order');
    }

}
