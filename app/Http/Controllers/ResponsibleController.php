<?php namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Responsible;
use App\User;
use App\Organization;
use View;
use Hash ; 
use Validator;
use Input;
use Session;
use Redirect;
use HTML;


class ResponsibleController extends Controller {



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        // get all the nerds

        $responsibles = Responsible::all();
        
        // load the view and pass the nerds
        return View::make('responsibles.index')
            ->with('responsibles', $responsibles);



	}


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $Organizations = Organization::orderBy('name', 'asc')->lists('name','id');
    
        if(!$Organizations ){
          
           $message=trans('tr.You have to add Organization first');
                return Redirect::to('organizations/create')
                ->withErrors(array('message' => "$message"));
        }

        return View::make('responsibles.create')
            ->with('Organizations', $Organizations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $rules = array(

            'name'       => 'required',
            'email'      => 'required|email|unique:users',
            'password' => 'required',
            'organization_id' => 'required|numeric'
        );


$requiredEmail = trans('validation.requiredEmail');
$requiredName = trans('validation.requiredName');
$requiredPassword = trans('validation.requiredPassword');
$unique= trans('validation.has been taken');

$email= trans('validation.The email must be a valid email address');
$messages = array(
                
    		'email.required' => "$requiredEmail   !",
		'name.required' => "$requiredName   !",
                'password.required' => "$requiredPassword  !",
    		'email.unique'    => " $unique ",
                'email'=>"$email"
		);

        $validator = Validator::make(Input::all(), $rules,$messages);


        // process the login
        if ($validator->fails()) {
	    // get the error messages from the validator
            $messages = $validator->messages();
            return Redirect::to('responsibles/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $user = new User;
            $user->name       = Input::get('name');
            $user->email      = Input::get('email');
            $user->password   = Hash::make(Input::get('password'));
            $user->type       = "3";
            $user->save();

            $responsible = new Responsible ;
            $responsible->user_id         = $user->id;
            $responsible->organization_id = Input::get('organization_id');
            $responsible->save();

            // redirect
 $message=trans('tr.Successfully created Responsible');
            Session::flash('message', "$message");
            return Redirect::to('responsibles');
        }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the admin

        $responsible = Responsible::find($id);

        // show the view and pass the admin to it
        return View::make('responsibles.show')
            ->with('responsible', $responsible);

	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id){
        // get the admin
        $responsible = Responsible::find($id);
        $Organizations = Organization::orderBy('name', 'asc')->lists('name','id');

        // show the edit form and pass the admin
        return View::make('responsibles.edit', compact('responsible', 'Organizations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
            'organization_id' => 'required|numeric'
        );
        
$requiredEmail = trans('validation.requiredEmail');
$requiredName = trans('validation.requiredName');
$email= trans('validation.The email must be a valid email address');
$messages = array(
                
    		'email.required' => "$requiredEmail   !",
		'name.required' => "$requiredName   !",

                'email'=>"$email"
		);

        $validator = Validator::make(Input::all(), $rules,$messages);
        // process the login
        if ($validator->fails()) {
             // get the error messages from the validator
            //$messages = $validator->messages();
            return Redirect::to('responsibles/' . $id . '/edit')
		->withErrors($validator)
                ->withInput(Input::except('password'));

        } else {
            // store

            $responsible = Responsible::find($id);
            $responsible->organization_id = Input::get('organization_id');
            $responsible->save();

            $user = User::find($responsible->user_id);
            $user->name       = Input::get('name');
            $user->email      = Input::get('email');

            $user->password   = Hash::make(Input::get('password'));

            $user->save();

            $message=trans('tr.Successfully updated Responsible');
            Session::flash('message'," $message");

            return Redirect::to('responsibles');
        }

	}






    public function destroy($id)
    {
        // delete

        
       

        	// delete
        $responsible = Responsible::find($id);
	$user_id=$responsible->id;
        $user=User::find($user_id);
        $responsible->delete();
        $user->delete();

        $message=trans('tr.Successfully deleted Responsible') ;
      
        Session::flash('message', "$message");
        return Redirect::to('responsibles');

}

}
