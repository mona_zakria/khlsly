<?php namespace App\Http\Controllers;

use App\Organization;
use App\Admin;
use App\User;
use Auth;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		//dd(Auth::user());
		return view('welcome');
	}

	public function test()
	{
			if(Auth::user()){
				dd("user login type = ".Auth::user()->type);
			}else {
				dd("no user loged noe");
			}


		//$org =  Admin::get()->first()->organization;
		//$org =  Organization::get()->first()->admins;

		$org =  Admin::get()->first()->user;
		//$org =  User::get()->first()->admins;

		dd($org);
		echo "lol";
		//return view('test',compact('org'));
	}
}
