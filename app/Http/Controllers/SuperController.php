<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use DB;
use Illuminate\Http\Request;
use App\Organization;

class SuperController extends Controller {

	/**
	 * Display super admin dashboard.
	 *
	 * @return Response
	 */
	public function dashboard()
	{

	$organization_names=array();
	$services_count= array();
	$organization_ids=array();

//$organizations = DB::table('organizations')->where('deleted_at',NULL)->get();
$organizations = Organization::all();
$organizations_no = count($organizations);
//return $organizations_no;
foreach ($organizations as $key => $organization) {
	
	$organization_name=$organization->name;
array_push($organization_names,$organization_name);


$organization_id= $organization->id;
array_push($organization_ids,$organization_id);

$services = DB::table('services')->where('organization_id', $organization->id)->where('deleted_at',NULL)->get();
$count=count($services);
array_push($services_count,$count);

}
$colors=array();

$color=["panel panel-yellow","panel panel-danger","panel panel-info","panel panel-warning","panel panel-red","panel panel-green","panel panel-success","panel panel-primary"];


$i=0;
while (count($colors) < $organizations_no)
{
array_push($colors,$color[$i]);
$i=$i+1;
if($i == 7)
$i=0;
}


		return View::make('supers.dashboard')
		->with(compact('organizations_no'))
		->with(compact('organization_names'))
		->with(compact('organization_ids'))
		->with(compact('services_count'))
		->with(compact('colors'));

	}

	
}
