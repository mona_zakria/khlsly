<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Log;
use DB;
use Session;
use Auth;

class logsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request)
	{
	
	$user_id= Auth::user()->id;
$responsible_id = DB::table('responsibles')->where('user_id', $user_id)->first()->id;



		        $comment = $request->input('comment');
		        $order_id = $request->input('order_id');
          
          if($request->input('act') == 2)
{   
DB::table('logs')->insert([
    ['change_date' => date("Y-m-d H:i:s"), 'status' => $request->input('act'), 'action_date' => $request->input('action_date'), 
    'order_id' => $order_id, 'created_at' => date("Y-m-d H:i:s") , 'updated_at' => date("Y-m-d H:i:s") , 'comment' => '', 'responsible_id' => $responsible_id

    ]
]);

}

          if($request->input('act') == 3)
{

DB::table('logs')->insert([
    ['change_date' => date("Y-m-d H:i:s"), 'status' => $request->input('act'), 'action_date' => NULL , 
    'order_id' => $order_id, 'created_at' => date("Y-m-d H:i:s") , 'updated_at' => date("Y-m-d H:i:s") , 'comment' => $request->input('comment')

    ]
]);
	
}


return redirect('orders/3');
 return "done succefully !";
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
