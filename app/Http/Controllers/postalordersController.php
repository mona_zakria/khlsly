<?php namespace App\Http\Controllers;

//use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
//use Illuminate\Http\Request;
use Html;
use View;
use App\Upload;
use App\Order;
use App\Log;
use Illuminate\Http\Request;

use App\Attribute;
use DB;
class postalordersController extends Controller {

public function index()	{

	$logs = DB::select( DB::raw("SELECT * FROM logs WHERE id IN (SELECT MAX(id) FROM logs GROUP BY order_id) AND status = 5 And deleted_at is NULL ") );

	if ( count($logs) >=1 ){
	  return view("postalorders.index",compact("logs"));	
	}
else{
 return view("postalorders.index");
}
}

public function deliverOrder($order_id)
{
/*
$log=Log::where("order_id",$order_id)->get()->first();
$status=6;
$log->status=$status;
$log->save();
*/
DB::table('logs')->insert([
    ['change_date' => date("Y-m-d H:i:s"), 'status' => '6',  
    'order_id' => $order_id, 'created_at' => date("Y-m-d H:i:s") , 'updated_at' => date("Y-m-d H:i:s") , 'comment' => ''

    ]
]);

return redirect('postalorders');

}

public function search(Request $request)
{
		$from = $request->input('from');
		$to = $request->input('to');

	$searched_orders = DB::select( DB::raw("SELECT * FROM logs WHERE id IN (SELECT MAX(id) FROM logs GROUP BY order_id) 
                            AND status =5 AND (action_date between '$from' and '$to' )") );

	if($searched_orders){

	return view("postalorders.index",compact("searched_orders"));
	}
	else{
		Session::flash('message',"There is no orders between those days  !");
		//return Redirect::to('postalorders');

	return view("postalorders.index",compact("searched_orders"));
	}
}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
