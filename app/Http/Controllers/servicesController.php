<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use Response;
use URL;
use Request;
use View;
use DB;
class servicesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


public function search()
	{
	
	$url = new URL;
    $query = Request::get('query');
  //$url->desc= $request->input('description');
//DB::select(DB::raw('RENAME TABLE photos TO images'));

$searched_services = DB::table('services')->where('name', 'like', "%$query%")->get(array('id','name','description'));

// foreach($searched_services as $searched_service)
// {
// print $searched_service->name;
// }


 return Response::json($searched_services);

		}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function add()
{
  return View::make('services.new');

}



	public function create()
	{
		    $organization_id = Request::get('organization_id');
		    $name = Request::get('name');
		    $description = Request::get('description');

		
	DB::table('services')->insert(
    ['name' => $name, 'description' => $description, 'organization_id' => $organization_id, 'created_at' => '0000-00-00 00:00:00' , 'updated_at' => '0000-00-00 00:00:00']
);
return "done successfully";

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	
	public function show($service_id)
	{
		
           $services=Service::where("id", $service_id)->get(array('id','name','description'));; 
	   return Response::json(array(
                                      
                                       'services' => $services));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
