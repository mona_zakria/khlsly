<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Postal;
use App\User;
use App\Organization;
use View;
use Hash ; 
use Validator;
use Input;
use Session;
use Redirect;
use HTML;


class PostalController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // get all the nerds
        $postals = Postal::all();
        
        // load the view and pass the nerds
        return View::make('postals.index')
            ->with('postals', $postals);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('postals.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{


		$rules = array(
            'name'       => 'required',
            'email'      => 'required|email|unique:users',
            'password' => 'required'
        );


$requiredEmail = trans('validation.requiredEmail');
$requiredName = trans('validation.requiredName');
$requiredPassword = trans('validation.requiredPassword');
$unique= trans('validation.has been taken');
$the= trans('validation.the');
$email= trans('validation.The email must be a valid email address');
$messages = array(
                
    		'email.required' => "$requiredEmail   !",
		'name.required' => "$requiredName   !",
                'password.required' => "$requiredPassword  !",
    		'email.unique'    => " $unique ",
                'email'=>"$email"
		);

        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
		 // get the error messages from the validator
            //$messages = $validator->messages();
            return Redirect::to('postals/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $user = new User;
            $user->name       = Input::get('name');
            $user->email      = Input::get('email');
            $user->password   = Hash::make(Input::get('password'));
            $user->type       = "4";
            $user->save();

            $postal = new Postal ;
            $postal->user_id         = $user->id;
            $postal->save();

            // redirect
            $message=trans('tr.Successfully created Postal!') ;
            Session::flash('message', " $message ");
            return Redirect::to('postals');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the postal
        $postal = Postal::find($id);

        // show the view and pass the postal to it
        return View::make('postals.show')
            ->with('postal', $postal);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		// get the postal
        $postal = Postal::find($id);
        
        // show the edit form and pass the postal
        return View::make('postals.edit', compact('postal'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
        // read more on validation at http://laravel.com/docs/validation
		$rules = array(
            'name'       => 'required',
            'email'      => 'required|email'
        );
$requiredEmail = trans('validation.requiredEmail');
$requiredName = trans('validation.requiredName');
$email= trans('validation.The email must be a valid email address');

$messages = array(
                
    		'email.required' => "$requiredEmail   !",
		'name.required' => "$requiredName   !",
                'email'=>"$email"
		);

        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
		 // get the error messages from the validator
           // $messages = $validator->messages();
            return Redirect::to('postals/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store

            $postal = Postal::find($id);

            $user = User::find($postal->user_id);
            $user->name       = Input::get('name');
            $user->email      = Input::get('email');
            $user->save();

            // redirect
            $message=trans('tr.Successfully updated Postal!');
            Session::flash('message',"$message " );
            return Redirect::to("postals/$id");
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	
      
	// delete
        $Postal =Postal ::find($id);
	$user_id=$Postal->id;
        $user=User::find($user_id);
        $Postal->delete();
        $user->delete();

        // redirect
        $message=trans('tr.Successfully deleted Postal!');
        Session::flash('message',"$message  ");
        return Redirect::to('postals');
	}

}
