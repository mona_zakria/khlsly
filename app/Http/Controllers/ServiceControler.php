<?php namespace App\Http\Controllers;

use App\Organization;
use App\User;
use App\Admin;
use App\Service;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use URL;
use Request;
use View;
use Session;
use Validator;
use Redirect;
use Auth;
use DB;
use Input;
use File;
class ServiceControler extends Controller {

public function __construct()
{

}

public function create()
{

  //$organization_id = Admin::where("user_id",Auth::user()->id)->first()->organization_id;
  return view('services.new');// ->with(compact('organization_id'));

}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(Request $request)
	{
	
	$rules = array(
            'name'       => 'required | unique:services',
            'description'      => 'required'   
        );



$requiredName = trans('validation.requiredName');
$requiredDescription = trans('validation.requiredDescription');
$uniqueName= trans('validation.uniqueName');
$messages = array(
                

		'name.required' => "$requiredName   !",
    		'name.unique'    => " $uniqueName ",
                'description.required' => "$requiredDescription  !"
       
		);

        $validator = Validator::make(Input::all(), $rules,$messages);
        // process the login
        if ($validator->fails()) {
	    // get the error messages from the validator
            //$messages = $validator->messages();
            return Redirect::to('service/create')
                ->withErrors($validator)
		 ->withInput(Input::except('password', 'password_confirm'));

        } else {
            
    		$organization_id = $organization_id = Admin::where("user_id",Auth::user()->id)->first()->organization_id;
    		$name = Input::get('name');
    		$description =  Input::get('description');
    // check if there is a service with the same name even in soft deleted services
                $service = Service::withTrashed()->where("name",$name)->first();

    		if( count($service) >= 1){
     
    			 Session::flash('message','this Service is already exists and may be in the soft deleted services!');
     			return Redirect::to("service");

    		}else{ 
			DB::table('services')->insert( ['name' => $name, 'description' => $description, 'organization_id' => $organization_id, 'created_at' => '0000-00-00 00:00:00' , 'updated_at' => '0000-00-00 00:00:00']);
			return Redirect::to("service");
		     }
              }
}



public function index()
{
		$organization_id = Admin::where("user_id",Auth::user()->id)->first()->organization_id;
		//dd($organization_id);
	    $services=Service::where("organization_id",$organization_id)->get();
		//dd($services);
	  if ( count($services) >=1 ){

	     return view("services.list",compact("services"));
           }
	else{
	    return view("services.list");
}

	}



public function index_super($id)
{
$organization_id=$id;

	    $services=Service::where("organization_id",$organization_id)->get();
		//dd($services);
	  if ( count($services) >=1 ){

	     return view("services.index_super",compact("services"));
           }
	else{
	    return view("services.index_super");
}

	}





public function show($id)
{
	$service=Service::whereId($id)->first();


        $organizationId=$service->organization_id;
	$organization=Organization::find($organizationId); 
//dd($organization);
        return view("services.show",compact("service","organization"));
}


public function show_super($id)
{
	$service=Service::whereId($id)->first();


        $organizationId=$service->organization_id;
	$organization=Organization::find($organizationId); 
//dd($organization);
        return view("services.show_service_super",compact("service","organization"));
}




public function list_services($id)
	{


    	$services=Service::where("organization_id", $id)->get(array('id','name','description'));; 
	    return Response::json(array(   'error' => false,
                                       'services' => $services),
                                        200
                                    );

	}


public function edit($serviceId){
		// get the service
        $service = Service::find($serviceId);

        // show the edit form and pass the service
        return view('services.edit', compact('service'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($serviceId)
	{
	$rules = array(
            'name'       => 'required',
            'description'      => 'required'   
        );

$requiredName = trans('validation.requiredName');
$requiredDescription = trans('validation.requiredDescription');
$messages = array(
                

		'name.required' => "$requiredName   !",
                'description.required' => "$requiredDescription  !"
       
		);

        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
	    // get the error messages from the validator
            $messages = $validator->messages();
            return Redirect::to('service/' . $serviceId . '/edit')
                ->withErrors($validator)
		 ->withInput(Input::except('password', 'password_confirm'));

        } else{
            // store

            $service = Service::find($serviceId);
            $service->name       = Input::get('name');
            $service->description  = Input::get('description');
            $service->save();

            return Redirect::to("service");
        }

	}

public function destroy($id)
{
		// delete
    $service = Service::find($id);
	$organization_id= $service->organization_id;
    $service->delete();

    return Redirect::to("service");
}


/**
* this function will return no of orders that has been taken by responsibles
*/
public function adminDashboard()
{
		
		  $organization_id= Admin::where("user_id",Auth::user()->id)->first()->organization_id;	
		  $statistics = DB::select( DB::raw("select count(orders.id) as no_of_orders , orders.service_id as service_id ,services.name as service_name ,services.organization_id, services.deleted_at,orders.deleted_at from  orders join services on  orders.service_id = services.id group by services.id having services.organization_id=". $organization_id." and services.deleted_at is NULL ") );


		//$colors=["panel panel-info","panel panel-green","panel panel-danger","panel panel-warning"];
 		$services=Service::where("organization_id",$organization_id)->get();
		//dd($services);
		$res = DB::table('responsibles')->where('organization_id', $organization_id)->get();
 		$responsibles= array();
		$counters= array();
		$names= array();


		foreach ($res as $key => $responsible) {
			if($responsible->organization_id == $organization_id)
			{
				array_push($responsibles,$responsible->id);
			}

		}

		foreach($responsibles as $responsible)

		{
			$user_id = DB::table('responsibles')->where('id', $responsible)->first()->user_id;

			$name= DB::table('users')->where('id', $user_id)->first()->name;
			array_push($names,$name);

			$orderNum = DB::select( DB::raw("select * from logs where responsible_id = $responsible and (status = 2 or status =3);") );

			$counter= count($orderNum);
			array_push($counters,$counter);
			$statisticsCount = count($statistics);


			$color=["panel panel-yellow","panel panel-green","panel panel-red","panel panel-info","panel panel-primary","panel panel-warning","panel panel-success","panel panel-danger"];

				$colors=array();
				$i=0;
				while (count($colors) < $statisticsCount)
				{
					array_push($colors,$color[$i]);
					$i=$i+1;
					if($i == 7)
					$i=0;
				}
		}


		//this  part will return the no  of rejected orders in each service

			$result= array();

			$service_names = array();

			foreach ($services as $index => $service) {
				array_push($service_names,$service->name);
				$orders = DB::table('orders')->where('service_id', $service->id)->get();

				$sum=array();

				foreach ($orders as $key => $order) {
						$logs= DB::table('logs')->where('order_id', $order->id)->where('status',"3")->get();

						$count_rejected_order=count($logs);
						array_push($sum, $count_rejected_order);

				}
						$res=array_sum($sum);
					array_push($result,$res);
		}

		$organization = DB::table('organizations')->where('id', $organization_id)->first();
		$organization_name=$organization->name;
	 	 if ( count($services) >=1 ){

	    		 return view("services.adminDashboard",compact("services","statistics","counters","names","colors","result","service_names","organization_name"));
				
           	}
		else{
	   		 return view("services.adminDashboard",compact("organization_name"));
		}


		//return view("services.statistics",compact("statistics"));


				

}

/*
public function serviceStatistics()
	{
		//
		  $organization_id = $organization_id = Admin::where("user_id",Auth::user()->id)->first()->organization_id;
			//echo $organization_id;
		  //$services=Service::where("organization_id",$organization_id)->get();
		 // echo $services;

		 // foreach($services as $service){
       			//dd($service->id);
		  $statistics = DB::select( DB::raw("select count(orders.id) as no_of_orders , orders.service_id ,services.name as service_name ,services.organization_id from  orders join services on  orders.service_id = services.id group by services.id having services.organization_id=". $organization_id."") );

                  // }

		//dd($statistics);
		 return view("services.statics",compact("statistics"));
		//return Response::json($statistics);
                  //return view("bar");
	}*/

/**
*this function will return the responsible  and the order which he is handling it rigth now 
*/

public function responsibleStatics(){

	$organization_id = $organization_id = Admin::where("user_id",Auth::user()->id)->first()->organization_id;	
	$responsibleStatics = DB::select( DB::raw("select s.id ,s.name as service,o.id order_id ,max(l.id),l.responsible_id,u.name  as responsible_name from services s, logs l, orders o,users u,responsibles r  where s.id = o.service_id and o.id = l.order_id and u.id=r.user_id and r.id=l.responsible_id  and s.deleted_at is NULL and o.deleted_at is NULL and l.status=8 and s.organization_id=".$organization_id."   group by l.responsible_id  ") );

/*
select s.id ,s.name,o.id,max(l.id),l.responsible_id from services s, logs l, orders o where s.id = o.service_id and o.id = l.order_id and s.organization_id=4 and l.status=8  group by l.responsible_id;*/

$staticCount = count($responsibleStatics);


$color=["panel panel-yellow","panel panel-green","panel panel-red","panel panel-info","panel panel-primary","panel panel-warning","panel panel-success","panel panel-danger"];



$colors=array();
//["panel panel-danger","panel panel-info","panel panel-yellow","panel panel-green","panel panel-red","panel panel-primary","panel panel-success","panel panel-warning"];

$i=0;
while (count($colors) < $staticCount)
{
array_push($colors,$color[$i]);
$i=$i+1;
if($i == 7)
$i=0;
}


		$organization = DB::table('organizations')->where('id', $organization_id)->first();

 if ( count($responsibleStatics) >=1 ){

return view("services.responsibleStatics",compact("responsibleStatics","colors","organization_name"));
           }
	else{

return view("services.responsibleStatics",compact("colors","organization_name"));
}



//dd($responsibleStatics);

}
 public function showOrder($order_id){



		$attributes=array();
				$attr=array();
$sent_files = array();

$ser_id = DB::table('orders')->where('id', $order_id)->first()->service_id;
$ser_name = DB::table('services')->where('id', $ser_id)->first()->name;

if (file_exists($order_id)) {

$files = File::allFiles($order_id);
foreach ($files as $file)
{
	array_push($sent_files, (string)$file);

}

  return View::make('services.showOrder')
// ->with(compact('attr'))
 ->with(compact('order_id'))
  ->with(compact('ser_name'))
 ->with(compact('sent_files'));
}
}


}

