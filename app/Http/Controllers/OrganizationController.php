<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Organization;
use View;
use Hash ; 
use Validator;
use Input;
use Session;
use Redirect;
use HTML;


use Response;
use URL;
use Request;





class OrganizationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
      
        $organizations = Organization::all();
        //dd($organizations);
        // load the view and pass the nerds
        return View::make('organizations.index')
            ->with('organizations', $organizations);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('organizations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{


		$rules = array(
            'name'       => 'required| unique:organizations',
            'description'      => 'required',
            'address' => 'required'
        );
$requiredName = trans('validation.requiredName');
$requiredDescription = trans('validation.requiredDescription');
$requiredAddress = trans('validation.requiredAddress');
$uniqueName=trans('validation.uniqueName');

$messages = array(
                

		'name.required' => "$requiredName   !",
		'name.unique'    => " $uniqueName ",
                'description.required' => "$requiredDescription  !",
    		'address.required'=> "$requiredAddress !"
                
		);

        $validator = Validator::make(Input::all(), $rules,$messages);



        // process the login
        if ($validator->fails()) {
            // get the error messages from the validator
            //$messages = $validator->messages();
            return Redirect::to('organizations/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $organization = new Organization;
            $organization->name       = Input::get('name');
            $organization->description = Input::get('description');
            $organization->address   = Input::get('address');
            $organization->save();

            // redirect
            $message=trans('tr.organization has been successfully created');
            Session::flash('message', "$message");
            return Redirect::to('organizations');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the postal
        $organization = Organization::find($id);

        // show the view and pass the postal to it
        return View::make('organizations.show')
            ->with('organization', $organization);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		
        $organization = Organization::find($id);
        
        // show the edit form and pass the organization
        return View::make('organizations.edit', compact('organization'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
        // read more on validation at http://laravel.com/docs/validation




$rules = array(
            "name"      => 'required',
            "description"      => 'required',
            "address" => 'required'
        );



$requiredName = trans('validation.requiredName');
$requiredDescription = trans('validation.requiredDescription');
$requiredAddress = trans('validation.requiredAddress');

$messages = array(
                

		'name.required' => "$requiredName   !",
                'description.required' => "$requiredDescription  !",
    		'address.required    '=> " $requiredAddress "
                
		);

        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
            // get the error messages from the validator
            //$messages = $validator->messages();
            return Redirect::to('organizations/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store

            $organization = Organization::find($id);
            $organization->name       = Input::get('name');
            $organization->address      = Input::get('address');
            $organization->description      = Input::get('description');
            $organization->save();

            // redirect
            $message=trans('tr.organization has been successfully updated');
            Session::flash('message',"$message" );
            return Redirect::to("organizations/$id");
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
        $organization = Organization::find($id);
        $organization->delete();

        // redirect
         $message=trans('tr.organization has been successfully deleted');
        Session::flash('message',"$message");
        return Redirect::to('organizations');
	}

/**
* Display a listing of the resource.
*
* @return Response
*/


public function listOrganizations()
{
$organizations = "";	
$url = new URL;
$url->action = Request::get('action');  //http://localhost:8000/organizations?action=list
if ($url->action == "list"){

//$organizations=Organization::all();//->lists('id','name','description');

$organizations=Organization::get(array('id','name','description', 'address'));

}


//$url->desc= $request->input('description');
//$url->save();
return Response::json(array('error' => false,'organizations' => $organizations),200);
}




}
