<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Service;
use App\User;
use App\Order;
use Organizations;
use Response;
use URL;
use Request;
use App\Admin;
use Auth;
use DB;
use View;
class testController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$url = new URL;
    $url->description = Request::get('description');
  //$url->desc= $request->input('description');
    //$url->save();
 
    return Response::json(array(
        'error' => false,
        'urls' => $url),
        200
    );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getRejectedOrders()
	{
	$result= array();

$service_names = array();

$user_id= Auth::user()->id;
$organization_id = DB::table('admins')->where('user_id', $user_id)->first()->organization_id;
$services= DB::table('services')->where('organization_id', $organization_id)->get();
foreach ($services as $index => $service) {
	array_push($service_names,$service->name);
$orders = DB::table('orders')->where('service_id', $service->id)->get();

	$sum=array();

foreach ($orders as $key => $order) {
	$logs= DB::table('logs')->where('order_id', $order->id)->where('status',"3")->get();


$count=count($logs);
array_push($sum, $count);

}
$r=array_sum($sum);
array_push($result,$r);
}



return View::make('test.test')
->with(compact('service_names'))
->with(compact('result'));





	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{

	$user_id= Auth::user()->id;
$organization_id = DB::table('admins')->where('user_id', $user_id)->first()->organization_id;
$res = DB::table('responsibles')->where('organization_id', $organization_id)->get();
 
$responsibles= array();
$counters= array();
$names= array();


foreach ($res as $key => $responsible) {
if($responsible->organization_id == $organization_id)
{
array_push($responsibles,$responsible->id);
}

}

foreach($responsibles as $responsible)

{
$user_id = DB::table('responsibles')->where('id', $responsible)->first()->user_id;

$name= DB::table('users')->where('id', $user_id)->first()->name;
array_push($names,$name);

	$orderNum = DB::select( DB::raw("select * from logs where responsible_id = $responsible and (status = 2 or status =3);") );

$counter= count($orderNum);
array_push($counters,$counter);

}

return View::make('test.test')
->with(compact('counters'))
->with(compact('names'));




	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
