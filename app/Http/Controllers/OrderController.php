<?php namespace App\Http\Controllers;

use App\Service;
use App\Attribute;
use App\Type;
use App\Order;
use App\Upload;
use App\Log;
use Input;
use Auth;
use Response;
class OrderController extends Controller {

public function __construct()
	{

	}

	public function index()
	{
		return 'orders';
	}



	public function postalOrders()
	{
	$status1=5;	
	$status2=6;	
          $logs=Log::where("status",$status1)->orWhere("status",$status2 )->get();
          return view("orders.undelivedOrders",compact("logs"));
	 
	}

	public function deliverOrder($order_id)
	{
		
         //$order_id = Input::get("order_id");
	 $log=Log::where("order_id",$order_id)->get()->first();
	
         $status=6;
	 $log->status=$status;
         $log->save();
         //return $log;
	//$log->save();
	// $logs=Log::where("status",5)->get();
  
               //return view("orders.undelivedOrders",compact("logs"));
       return redirect('undelivedOrders');
	}


public function create($service_id)
	{


		
		$atrributes=Attribute::where("service_id",$service_id)->get();
        //$atrributes= $this->atrributes;
         
        return view("orders.add",compact("atrributes"));
	}



public function store($service_id)
	{

		    //store orders
			$order = new Order;
			$order->requester_id = Auth::user()->id;
			$order->service_id =$service_id ;
			$order->date=date("Y-m-d H:i:s");
			$order->save();


            $atrributes=Attribute::where("service_id",$service_id)->get();
           // $atrributes= $this->atrributes;

             $uploads = Input::all();
            // foreach ( $uploads as $key => $value) {
        	
        	//echo "key = ".$key."  value = ".$value."<br >";
        	/*$filename = $uploads[$key]->getClientOriginalName();
			$destinationPath = 'uploads/'.str_random(8);
			$upload->value = $filename; 
			$uploadSuccess = $file->move($destinationPath, $filename);
*/


      //  }

 		foreach($atrributes as $atrribute) {

        $upload = new Upload;
		$upload->order_id=$order->id;
		//echo Input::get($atrribute->name);
		$atrribute->name = preg_replace('/\s+/', '_', $atrribute->name);

		if ($atrribute->type->name == "file"){
            if (Input::hasFile($atrribute->name))
            {
    
			$filename = $uploads[$atrribute->name]->getClientOriginalName();
			//$filename=$atrribute->id. '.' . $uploads[$atrribute->name]->getClientOriginalExtension();
			//$destinationPath = 'uploads/';
			$upload->value = $filename;
			}
			else{
			$upload->value ="";	
			} 
			//.$atrribute->id;
			//$uploadSuccess = $uploads[$atrribute->name]->move($destinationPath, $filename);

		}else{
		
			//echo $atrribute->name;
		$upload->value=$uploads[$atrribute->name];	
		}
		

 		$upload->attribute_id=$atrribute->id;
		echo   $upload->attribute_id."      ". $upload->value."<br >";
		$upload->save();


		if ($atrribute->type->name == "file"){
             if (Input::hasFile($atrribute->name))
            {
    
			//$filename = $uploads[$atrribute->name]->getClientOriginalName();
			$filename=$upload->id. '.' . $uploads[$atrribute->name]->getClientOriginalExtension();


			$upload->value=$filename;
			$upload->save();
			$destinationPath = 'uploads/';
			//$upload->value = $filename;
			$uploadSuccess = $uploads[$atrribute->name]->move($destinationPath, $filename);
           }
		}

        }	
        //save in table logs
		$log = new Log;
		$log->order_id=$order->id;
		$log->change_date=date("Y-m-d H:i:s");
		$status=1;
		$log->status=$status; 
	    $log->address=Input::get("address");
	     $log->save();

	}
public function requesterOrders($requester_id){

          // $url = new URL;
          // $url->id = Request::get('requester_id');
           $orders=Order::where("requester_id",$requester_id)->get(array('id','date','service_id'));
           foreach ($orders as $order){
           $order_id=$order->id;
          // $status=Log::where("order_id",$order_id)->get(array('status'));
	   $order->status = Log::where("order_id",$order_id)->get(array('status'))->last()->status; 
           //$orders->name;
}
	   return Response::json(array(
                                       'error' => false,
                                       'orders' => $orders),
                                       200
);
}


}
