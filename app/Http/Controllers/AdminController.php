<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;
use App\User;
use App\Organization;
use View;
use Hash ; 
use Validator;
use Input;
use Session;
use Redirect;
use HTML;
use JsValidator;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // get all the nerds
        $admins = Admin::all();
       // dd($admins);
        // load the view and pass the nerds
        return View::make('admins.index')
            ->with('admins', $admins);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $Organizations = Organization::orderBy('name', 'asc')->lists('name','id');

    	//dd($Organizations);


        if(!$Organizations ){
            return Redirect::to('organizations/create')
                ->withErrors(array('message' => "{{ trans('tr.You have to add Organization first') }}" ));
        }

		return View::make('admins.create')
            ->with('Organizations', $Organizations);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	$rules = array(
            "name"  => 'required',
            "email"      => 'required|email| unique:users',
            "password" => 'required',
            "organization_id" => 'required|numeric'
        );

$requiredEmail = trans('validation.requiredEmail');
$requiredName = trans('validation.requiredName');
$requiredPassword = trans('validation.requiredPassword');
$unique= trans('validation.has been taken');
$the= trans('validation.the');
$email= trans('validation.The email must be a valid email address');
$messages = array(
                
    		'email.required' => "$requiredEmail   !",
		'name.required' => "$requiredName   !",
                'password.required' => "$requiredPassword  !",
    		'email.unique'    => " $unique ",
                'email'=>"$email"
		);

        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
       
	
            return Redirect::to('admins/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $user = new User;
            $user->name       = Input::get('name');
            $user->email      = Input::get('email');
            $user->password   = Hash::make(Input::get('password'));
            $user->type       = "2";
            $user->save();

            $admin = new Admin ;
            $admin->user_id         = $user->id;
            $admin->organization_id = Input::get('organization_id');
            $admin->save();

            // redirect 
            $message=trans('tr.Successfully created Admin!');
            Session::flash('message'," $message");
            return Redirect::to('admins');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get the admin
        $admin = Admin::find($id);

        // show the view and pass the admin to it
        return View::make('admins.show')
            ->with('admin', $admin);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id){
		// get the admin
        $admin = Admin::find($id);
        $Organizations = Organization::orderBy('name', 'asc')->lists('name','id');

        // show the edit form and pass the admin
        return View::make('admins.edit', compact('admin', 'Organizations'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// validate
        // read more on validation at http://laravel.com/docs/validation
		$rules = array(
            'name'       => 'required',
            'email'      => 'required|email',
            'organization_id' => 'required|numeric'
        );

        $requiredEmail = trans('validation.requiredEmail');
	$requiredName = trans('validation.requiredName');
	$email= trans('validation.The email must be a valid email address');

$messages = array(
                
    		'email.required' => "$requiredEmail   !",
		'name.required' => "$requiredName   !",
                'email'=>"$email"
		);

        $validator = Validator::make(Input::all(), $rules,$messages);

        // process the login
        if ($validator->fails()) {
	  // get the error messages from the validator
            $messages = $validator->messages();
            return Redirect::to('admins/' . $id . '/edit')
                ->withErrors($validator);
        } else {
            // store

            $admin = Admin::find($id);
            $admin->organization_id = Input::get('organization_id');
            $admin->save();

            $user = User::find($admin->user_id);
            $user->name       = Input::get('name');
            $user->email      = Input::get('email');
            $user->save();

            // redirect
	    $message=trans('tr.Successfully updated Admin!');
            Session::flash('message',"$message");
            return Redirect::to("admins/$id");
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
        $admin = Admin::find($id);
	$user_id=$admin->id;
        $user=User::find($user_id);
        $admin->delete();
        $user->delete();

        // redirect
        $message=trans('tr.Successfully deleted the Admin!');
        Session::flash('message', "$message");
        return Redirect::to('admins');
	}

}
