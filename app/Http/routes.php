<?php

/*
1 SuperAdmin
2 Admin 
3 Responsible
4 Postal 
5 Requester
*/

//->whereNull('deleted_at')


// status :
// 1-- new 
// 2-- accepted 
// 3-- rejected 
// 4-- 
// 5-- 
// 6-- 
// 7-- updated 
// 8-- locked 
// 9-- howa 7iwslo
// 10-- to be delivered

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
Route::get('newservice/{organization_id}', 'servicesController@add');
Route::get('addservice', 'servicesController@create');
Route::get('showservice/{service_id}', 'servicesController@show'); //done 
Route::get('search', 'servicesController@search');
Route::get('orders', 'ordersController@index');
Route::get('test', 'testController@index');
*/


/////////////////////////////// Android //////////////////////////

Route::get('/userOrder', 'ordersController@getUserOrder');
Route::get('userOrders', 'ordersController@getUserOrders');
Route::get('editOrder', 'ordersController@edit');
Route::get('addOrder', 'ordersController@addOrder');




//////////////////////////////
Route::get('test', 'testController@show');
Route::get('/', 'HomeController@dashboard');
Route::get('/statics', 'testController@statistics');  

Route::get('/responsibleStatics', 'testController@responsibleStatics');




Route::get('rejectedOrders', 'testController@getRejectedOrders');


Route::get('/', 'HomeController@dashboard');
Route::get('/statics', 'testController@statistics');



//Route::get('statis', 'testController@show');



Route::group(array('before' => 'is_postal'), function() {
	Route::get('/postaldash', 'postalordersController@index');
        Route::get('postalorders', 'postalordersController@index');
        Route::get('postalsearch', 'postalordersController@search');
        Route::post('postalorders/{order_id}/delivered','postalordersController@deliverOrder');
});


Route::get('home', 'HomeController@index');

Route::get('servicesss/{id}', 'ServiceControler@list_services'); //android

Route::controllers([
  'auth' => 'Auth\AuthController',
  'password' => 'Auth\PasswordController',
]);





Route::filter('is_super_Admin', function(){ 

  if (!( Auth::user() && Auth::user()->type == 1)) {

     return Redirect::to('/home'); 

   }

});



Route::filter('is_admin', function(){

  if (!( Auth::user() && Auth::user()->type == 2)) {

     return Redirect::to('/home'); 

   }

});


Route::filter('is_responsible', function(){ 

  if (!( Auth::user() && Auth::user()->type == 3)) {

     return Redirect::to('/home'); 

   }
   
});

Route::filter('is_postal', function(){ 

  if (!( Auth::user() && Auth::user()->type == 4)) {

     return Redirect::to('/home'); 

   }
   
});

Route::filter('is_requester', function(){ 

  if (!( Auth::user() && Auth::user()->type == 5)) {

     return Redirect::to('/home'); 
   
   }

});



Route::group(array('before' => 'is_admin'), function() {

Route::resource('service', 'ServiceControler');
Route::get('/showOrder{id}', 'ServiceControler@showOrder');
//Route::get('/service', 'ServiceControler@index');
Route::get('/admindash', 'ServiceControler@adminDashboard');
Route::get('/responsibleStatics', 'ServiceControler@responsibleStatics');
});


Route::group(array('before' => 'is_super_Admin'), function() {

Route::resource('admins', 'AdminController');
Route::get('service_super/{id}', 'ServiceControler@index_super');
Route::get('show_super/{id}', 'ServiceControler@show_super');

Route::resource('responsibles', 'ResponsibleController');

Route::resource('postals', 'PostalController');

Route::resource('organizations', 'OrganizationController');

Route::get('/dashboard', 'SuperController@dashboard');

});


Route::group(array('before' => 'is_requester'), function() {

Route::get('services/{id}', 'ServiceControler@show');
Route::get('services/{id}/orders/add', 'OrderController@create');
Route::post('services/{service_id}/orders/new','OrderController@store');

});

Route::group(array('before' => 'is_responsible'), function() {
Route::get('/responsibledash', 'ordersController@home');
Route::get('orders/{id}', 'ordersController@index');
Route::get('show{id}', 'ordersController@show');
Route::get('f_status', 'logsController@edit');

});



Route::get('listorganizations', 'OrganizationController@listOrganizations');
//Route::get('organizations/add', 'OrganizationController@create');
//Route::Post('organizations/new', 'OrganizationController@store');

Route::get('orders/{requester_id}/list', 'OrderController@requesterOrders');
//Route::get('undelivedOrders', 'OrderController@undelivedOrders');
 //Route::post('orders/{order_id}/delivered','OrderController@deliverOrder');
