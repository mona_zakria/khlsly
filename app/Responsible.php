<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Responsible extends Eloquent {

	use SoftDeletes;
	
	public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
