<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Eloquent {

    use SoftDeletes;

	public function service()
    {
        return $this->belongsTo('App\Service');
    }

	public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }
    

}
