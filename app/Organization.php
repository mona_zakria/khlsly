<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Eloquent {


    use SoftDeletes;

   public function admins()
    {
        return $this->hasMany('App\Admin');
    }
   public function services()
    {
        return $this->hasMany('App\Service');
    }
    public function responsibles()
    {
        return $this->hasMany('App\Responsible');
    }


 public static function boot()
    {
        parent::boot();    
    
        // cause a delete of a product to cascade to children so they are also deleted
        static::deleted(function($organization)
        {
            $organization->admins()->delete();
            $organization->services()->delete();
            $organization->responsibles()->delete();
        });
    }    
}
