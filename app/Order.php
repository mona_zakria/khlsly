<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Eloquent {

    use SoftDeletes;
    
	public function requester()
    {
        return $this->belongsTo('App\Requester');
    }

	public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    public function logs()
    {
        return $this->hasMany('App\Log');
    }

public static function boot()
    {
        parent::boot();    
    
        // cause a delete of a product to cascade to children so they are also deleted
        static::deleted(function($order)
        {
            $order->logs()->delete();
           
        });
    }    
}
