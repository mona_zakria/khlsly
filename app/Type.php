<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Eloquent {

	use SoftDeletes;
	
	public function attributes()
    {
        return $this->hasMany('App\Attribute');
    }

}
