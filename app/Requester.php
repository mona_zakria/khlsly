<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Requester extends Eloquent {

	use SoftDeletes;
	
	public function orders()
    {
        return $this->hasMany('App\Order');
    }

	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
