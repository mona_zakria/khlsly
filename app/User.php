<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword , SoftDeletes ;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function supers()
    {
        return $this->hasMany('App\Super');
    }

    public function admins()
    {
        return $this->hasMany('App\Admin');
    }

	public function postals()
    {
        return $this->hasMany('App\Postal');
    }

	public function responsibles()
    {
        return $this->hasMany('App\Responsible');
    }

    public function requesters()
    {
        return $this->hasMany('App\Requester');
    }
public static function boot()
    {
        parent::boot();    
    
        // cause a delete of a product to cascade to children so they are also deleted
        static::deleted(function($user)
        {
            $user->admins()->delete();
            $user->requesters()->delete();
            $user->responsibles()->delete();
	   $user->postals()->delete();
        });
    }    


}
