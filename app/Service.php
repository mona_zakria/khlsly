<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Eloquent {

    use SoftDeletes;
    
	public function organization()
    {
        return $this->belongsTo('App\Organization');
    }
    
    public function attributes()
    {
        return $this->hasMany('App\Attribute');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

public static function boot()
    {
        parent::boot();    
    
        // cause a delete of a product to cascade to children so they are also deleted
        static::deleted(function($service)
        {
            $service->orders()->delete();
           
        });
    }    

}
