<?php namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Super extends Eloquent {

	use SoftDeletes;
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
