<?php

return [
'you must fill' => 'يجب عليك ادخال  ',
"requiredEmail" =>"  يجب عليك ادخال الايميل ",
"requiredName" =>"  يجب عليك ادخال الاسم ",
"requiredPassword" =>"  يجب عليك ادخال كلمة المرور ",
'Successfully created Admin!' => 'تم اضافة الادمن بنجاح',
'has been taken' => 'تم ادخال هذا الايميل من قبل',
'the' => 'هذا', 
 'name' => ' الاسم ',
 'Password' => ' كلمة المرور',
 'Email' => '  البريد الالكترونى',
'The email must be a valid email address' =>"رجاء إدخال عنوان بريد إلكتروني صحيح",
'requiredAddress' => "يجب عليك ادخال العنوان ",
'requiredDescription' => "يجب عليك ادخال الوصف ",
'uniqueName'=>'تم ادخال هذا الاسم من قبل',
];
