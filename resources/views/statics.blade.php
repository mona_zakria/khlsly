<html>
<head>

 <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--for morris shart-->

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


</head>

<body>


<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Statistics Of Services </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
<div>
<div class="row">
@foreach($statistics as $statistic)
<div class="col-md-4">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><h4>Number of Orders: {{ $statistic->no_of_orders }}</h4></div>
                                    <div><h3>{{ $statistic->service_name }}</h3></div>
                                </div>
                            </div>
</div>
</div></div>
@endforeach
</div> <!--row -->
</div>
<div id="myfirstchart" style="height: 250px;"></div>
</body>
<script>
var dataArray = new Array();
var data = <?php echo json_encode($statistics );?>; 
//console.log(data);

for(var object in data){

//for (var key in data[object] ) {
//console.log(data[object][key]);
dataArray.push({service:data[object]['service_name'],no_of_orders: data[object]['no_of_orders']});
//}
}

console.log(dataArray);

new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:dataArray,
hoverCallback: function(index, options, content) {
        return(content);
},
  // The name of the data record attribute that contains x-values.
  xkey: 'service',
 //labels: ['Service']
  // A list of names of data record attributes that contain y-values.
  ykeys: ['no_of_orders'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['no_of_orders'],
   parseTime: false

//labels: ['Orders', 'Service']
 // hideHover: false,

});

</script>
