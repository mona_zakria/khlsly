<html>
<head>

 <!-- Bootstrap Arabic CSS -->
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	 <script src="../../ckeditor/ckeditor.js"></script>    

</head>

<body>
  <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">{{ trans('tr.Home') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('responsibles') }}"><i class="fa fa-table fa-fw"></i> {{ trans('tr.Responsibles') }}</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('responsibles/create') }}"><i class="fa fa-edit fa-fw"></i>{{ trans('tr.Add Responsible') }}</a>
                        </li>

                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"> {{ trans('tr.Edit') }} {!! $responsible->user->name !!} </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
<div>
<div class="row">

<div class="col-md-12">
 @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
@endif


{!! Form::model($responsible, array('route' => array('responsibles.update', $responsible->id), 'method' => 'PUT')) !!}
    <div class="form-group">
       <b>{{ trans('tr.name') }}</b>
        {!! Form::text('name', $responsible->user->name, array('class' => 'form-control')) !!}
    </div>
    <div class="form-group">
         <b>{{ trans('tr.Email') }}</b>
        {!! Form::email('email', $responsible->user->email, array('class' => 'form-control')) !!}
    </div>

    <div class="form-group">
         <b>{{ trans('tr.Organization') }}</b>
        {!! Form::select('organization_id', $Organizations, $responsible->organization_id, array('class' => 'form-control')) !!}
    </div>
    {!! Form::submit('عدل مسؤل الطلبات!', array('class' => 'btn btn-primary')) !!}


{!! Form::close() !!}

</div>

</div> <!--row -->
</div>
</div>
    <!-- /#wrapper -->
  <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
</body>
