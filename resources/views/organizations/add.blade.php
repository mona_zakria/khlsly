<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

    <!--ckeditor -->
    <script src="../ckeditor/ckeditor.js"></script>
        
    
</head>
<body>
<div class="container">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Laravel</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ URL::to('organizations') }}">Organizations</a></li>

                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/auth/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>


{!! Form::open(array('url' => 'organizations')) !!}
<div class="form-group">
        {!! Form::label('name', "Name Of oragnization") !!}
        {!! Form::text('Name', $value = "",  $attributes = array('class' => 'form-control')) !!}
</div>
 <div class="form-group">
        {!! Form::label('Address', 'Address') !!}
        {!! Form::text('Address', $value = "",  $attributes = array('class' => 'form-control')) !!}
 </div>
<div class="form-group">
        {!! Form::label('Description', 'Description') !!}
        {!! Form::textarea('Description', $value = "" ,  $attributes = array('class'=>"ckeditor")) !!}

            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
              CKEDITOR.replace( 'Description' );

		
             
            </script>
</div>

{!! Form::submit('Add the Organization!', array('class' => 'btn btn-primary')) !!}
{!! Form::Close() !!}



</div>
</body>
