

<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    

</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Laravel</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ URL::to('organizations') }}">Organizations</a></li>
                    <li><a href="{{ URL::to('organizations/create') }}">Add Organization</a>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/auth/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<div class="container">


<h1>All the Organizations</h1>
{!! $errors->first("message") !!}
<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr class="info">
            <th>Name</th>
            <th>Address</th>
            <th>Description</th>
	    <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($organizations as $key => $value)
        <tr>
            <td>{{ $value->name }}</td>
            <td>{{ $value->address }}</td>
            <td>{!! $value->description !!}</td>

            <td>
                <a class="btn btn-mini btn-success" href="{{ URL::to('organizations/' . $value->id) }}">Show</a>

                <!-- edit this nerd (uses the edit method found at GET /admins/{id}/edit -->
                <a class="btn btn-mini btn-info" href="{{ URL::to('organizations/' . $value->id . '/edit') }}">Edit</a>

                {!! Form::open(array('route' => array('organizations.destroy', $value->id), 'method' => 'delete' , 'style'=>'display: inline;')) !!}
                    <button type="submit" class="btn btn-danger btn-mini" style="display: inline;" >Delete</button>
                {!! Form::close() !!}

               
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>
