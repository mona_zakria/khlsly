<!DOCTYPE html>
<html>
<head>
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="container">


<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Laravel</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/postalorders') }}">Home</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>



	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>	


<div id="i" >
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (isset($searched_orders))

<table class="table table-striped table-bordered">
  <thead>
        <tr class="info">
            <th>Order Id</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
 <tbody>

<tr>
@foreach($searched_orders as $log)
  <td>{{ $log->order_id }}</td>
  <td>{{ $log->address }}</td>


@if ( $log -> status == 5)

{!! Form::open(array( 'url' => "postalorders/$log->order_id/delivered",
    'method' => 'post')) !!}
<td><button type="submit" class="btn btn-small btn-info deliver" style="display: inline;" >Deliver Order</button></td>
{!! Form::Close() !!}
@else


       
   

@endif

 </tr>

@endforeach

@endif




<form action="postalsearch" method="get">


from : <input id = "from" type="date" name="from"  >
to : <input id = "to" type="date" name="to" >


<br>
<br>

<input type="submit" value="search">



</form>

</div>





@if (isset($logs))

<table class="table table-striped table-bordered">
  <thead>
        <tr class="info">
            <th>Order Id</th>
            <th>Address</th>
            <th>Action</th>
        </tr>
    </thead>
 <tbody>
@foreach($logs as $log)
<tr>

  <td>{{ $log->order_id }}</td>
  <td>{{ $log->address }}</td>


@if ( $log -> status == 5)

{!! Form::open(array( 'url' => "postalorders/$log->order_id/delivered",
    'method' => 'post')) !!}
<td><button type="submit" class="btn btn-small btn-info deliver" style="display: inline;" >Deliver Order</button></td>
{!! Form::Close() !!}
@else


       
   

@endif

 </tr>
@endforeach
@else
<p><h3>There is no orders to be delivered </h3></p>

@endif




 </tbody>
</table>
</div>
</body>
</html>
