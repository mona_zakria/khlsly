<!DOCTYPE html>
<html>
<head>

   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
</head>
<body>
   <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Laravel</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ URL::to('service') }}">services</a></li>
                    <li><a href="{{ URL::to('service/create') }}">Add Service</a>
		   <li><a href="{{ URL::to('/admindash') }}">Services Statistics</a>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/auth/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

<div class="container">

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (isset($services))

<table class="table table-striped table-bordered">
  <thead>
        <tr class="info">
            <th>Service Name</th>
            <!--th>Service Description</th-->
             <th>Action</th>
        </tr>
    </thead>
 <tbody>


@foreach($services as $service)
<tr>
  <td>{{ $service -> name }}</td>
  <!--td>{{ $service -> description }}</td-->
  <td>


                <a class="btn btn-mini btn-success" href="{{ URL::to('service/' . $service->id) }}">Show</a>
                <a class="btn btn-mini btn-info" href="{{ URL::to('service/' . $service->id . '/edit') }}">Edit</a>
		

  {!! Form::open(array('route' => array('service.destroy', $service->id), 'method' => 'delete' , 'style'=>'display: inline;')) !!}
                    <button type="submit" class="btn btn-danger btn-mini" style="display: inline;" >Delete</button>
                {!! Form::close() !!}
             

</td>
{!! Form::Close() !!}
 </tr>
@endforeach
@else

<p><h3>There is no services </h3></p>
<b>You can </b><a href="{{ URL::to('service/create') }}">Add Service</a>

@endif


</div>
</body>
</html>
