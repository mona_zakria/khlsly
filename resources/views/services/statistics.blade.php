<html>
<head>

<style>
    .an
{
color: white !important;}
</style>



    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
 <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

</head>

<body>
<nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Laravel</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ URL::to('service') }}">services</a></li>
                    <li><a href="{{ URL::to('service/create') }}">Add Service</a>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/auth/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Statistics Of Services </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
<div>
<div class="row">

@foreach($statistics as $statistic)

<div class="col-md-4">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge" ><a href="{{ url('/service/'. $statistic->service_id.'') }}" class= "an" ><h4>{{ $statistic->service_name }}</h4></a></div>
                                    <div><h4>Number of Orders: {{ $statistic->no_of_orders }}</h4></div>
                                </div>
                            </div>
</div>
</div></div>
@endforeach

</div> <!--row -->
</div>
<div id="myfirstchart" style="height: 250px;"></div>
<div id="myfirstchart2" style="height: 250px;"></div>
<div class="row">
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (isset($services))

<table class="table table-striped table-bordered">
  <thead>
        <tr class="info">
            <th>Service Name</th>
            <!--th>Service Description</th-->
             <th>Action</th>
        </tr>
    </thead>
 <tbody>


@foreach($services as $service)
<tr>
  <td>{{ $service -> name }}</td>
  <!--td>{{ $service -> description }}</td-->
  <td>


                <a class="btn btn-mini btn-success" href="{{ URL::to('service/' . $service->id) }}">Show</a>
                <a class="btn btn-mini btn-info" href="{{ URL::to('service/' . $service->id . '/edit') }}">Edit</a>
		

  {!! Form::open(array('route' => array('service.destroy', $service->id), 'method' => 'delete' , 'style'=>'display: inline;')) !!}
                    <button type="submit" class="btn btn-danger btn-mini" style="display: inline;" >Delete</button>
                {!! Form::close() !!}
             

</td>
{!! Form::Close() !!}
 </tr>
@endforeach
@else

<p><h3>There is no services </h3></p>
<b>You can </b><a href="{{ URL::to('service/create') }}">Add Service</a>

@endif
</div>
</body>
<script>
var dataArray = new Array();
var data = <?php echo json_encode($statistics );?>; 
//console.log(data);

for(var object in data){

//for (var key in data[object] ) {
//console.log(data[object][key]);
dataArray.push({service:data[object]['service_name'],no_of_orders: data[object]['no_of_orders']});
//}
}

console.log(dataArray);

new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:dataArray,
hoverCallback: function(index, options, content) {
        return(content);
},
  // The name of the data record attribute that contains x-values.
  xkey: 'service',
 //labels: ['Service']
  // A list of names of data record attributes that contain y-values.
  ykeys: ['no_of_orders'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['orders'],
   parseTime: false

//labels: ['Orders', 'Service']
 // hideHover: false,

});


var data1 = <?php echo json_encode($counters);?>;
var data2 = <?php echo json_encode($names);?>;
var dataArr2 = new Array();
for (var key in data1){
dataArr2.push({ Date: data2[key], value: data1[key] });
}
console.log(dataArr2);


new Morris.Line({


  element: 'myfirstchart2',


data: dataArr2 ,


  // The name of the data record attribute that contains x-values.
  xkey: 'Date',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['No. of orders','responsible'],
  parseTime: false
});


</script>
</html>

