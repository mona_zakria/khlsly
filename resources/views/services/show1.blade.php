
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
</head>
<body>
 <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Laravel</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ URL::to('service') }}">Services</a></li>
                    <li><a href="{{ URL::to('/service/create') }}">Add Service</a>
		   <li><a href="{{ URL::to('/admindash') }}">Services Statistics</a>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        <li><a href="{{ url('/auth/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<div class="container" style="background-color: #efeff2;">


<div class="span12" style="background-color: ##7df9ff;
               box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;"><h3>Service Name :</h3> {{ $service -> name }}</div>
<div class="span12" style="background-color: ##7df9ff;
               box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;"><h3>Service Description :</h3>
                  {{ $service->description; }}</div>
<div class="span12" style="background-color: ##7df9ff;
               box-shadow: inset 1px -1px 1px #444, inset -1px 1px 1px #444;"><h3>Organization Name : </h3>
            {{ $organization->name }}</div>
</div>
</body>
</html>

