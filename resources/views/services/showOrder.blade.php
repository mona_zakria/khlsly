<html>
	<head>

<!-- Bootstrap Arabic CSS -->
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  






<link rel="stylesheet" href="easy/css/example.css" />
  <link rel="stylesheet" href="easy/css/pygments.css" />
  <link rel="stylesheet" href="easy/css/easyzoom.css" />

 
	</head>
	<body>

 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">{{ trans('tr.Home') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/service') }}"><i class="fa fa-table fa-fw"></i> {{ trans('tr.Services') }}</a>
                        </li>
                        <li>
                            <a href="{{ url('/service/create') }}"><i class="fa fa-edit fa-fw"></i>{{ trans('tr.Add Service') }}</a>
                        </li>

                        </li>
                        <li>
                            <a href="{{ url('/responsibleStatics') }}"><i class="fa fa-pencil fa-fw"></i> {{ trans('tr.Statistics Of Responsibles') }}</a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>




<div id="page-wrapper">
          

<div class="row">

<div class="col-md-12 ">
   <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="color:blue;" >{{ trans('tr.Order of') }} {{ $ser_name }} </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
@foreach ($sent_files as $index => $upload )

<!-- <a data-lightbox="{{ $upload }}"  class="image-thumb" href="{{ $upload }}">
 -->
<div class="easyzoom easyzoom--overlay">
        <a href="{{ $upload }}">
          <img src="{{ $upload }}" alt="" width="500" height="500" />
        </a>
      </div>



<br>
@endforeach




 
  


</div> <!--row -->
</div>
</div>
    <!-- /#wrapper -->


</script>


  <script src="easy/dist/easyzoom.js"></script>
  <script>
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Get an instance API
    var api = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    // Setup thumbnails example
    $(".thumbnails").on("click", "a", function(e) {
      var $this = $(this);

      e.preventDefault();

      // Use EasyZoom's `swap` method
      api.swap($this.data("standard"), $this.attr("href"));
    });
  </script>


<script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/')}}/arabic/js/bootstrap-arabic.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>


	</body>    
</html>
