<html>
<head>

 <!-- Bootstrap Arabic CSS -->
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />
    <!--link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"-->

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
</head>

<body>
  <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/admindash') }}">{{ trans('tr.Admin Dashboard') }}</a>
            </div>
            <!-- /.navbar-header -->

<ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i>{{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/admindash') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/service') }}"><i class="fa fa-table fa-fw"></i> {{ trans('tr.Services') }}</a>
                        </li>
                        <li>
                            <a href="{{ url('/service/create') }}"><i class="fa fa-edit fa-fw"></i>{{ trans('tr.Add Service') }}</a>
                        </li>

                        </li>
                        <li>
                            <a href="{{ url('/responsibleStatics') }}"><i class="fa fa-pencil fa-fw"></i> {{ trans('tr.Statistics Of Responsibles') }}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>


<div id="page-wrapper">
  <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Current') }}<font color="green"> <br>{{ $organization_name }} </font></h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

@if (isset($services))
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Statistics of services') }} </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

<div class="row">
@foreach($statistics as $index => $statistic)
<div class="col-md-4">
                    <div class="{{ $colors[$index] }}">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><h4>{{ trans('tr.Service Name:') }}</h4><a href="{{ url('/service/'. $statistic->service_id.'') }}"> <h3>{{ $statistic->service_name }}</h3></a></div>
                                    <div><h4>{{ trans('tr.Number of Orders') }} {{ $statistic->no_of_orders }}</h4></div>
                                </div>
                            </div>
</div>
</div></div>
@endforeach
</div> <!--row -->
 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Relation between Services  and Orders') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

<div id="myfirstchart" style="height: 250px;"></div>
 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Relation between Resbonsible  and Orders') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>



<div id="myfirstchart2" style="height: 250px;"></div>


<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.statistics of rejected services') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>



<div id="myfirstchart3" style="height: 250px;"></div>


 <!-- /#wrapper -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script>
var dataArray = new Array();
var data = <?php echo json_encode($statistics );?>; 
//console.log(data);

for(var object in data){

//for (var key in data[object] ) {
//console.log(data[object][key]);
dataArray.push({service:data[object]['service_name'],no_of_orders: data[object]['no_of_orders']});
//}
}

console.log(dataArray);

new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'myfirstchart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data:dataArray,
hoverCallback: function(index, options, content) {
        return(content);
},
  // The name of the data record attribute that contains x-values.
  xkey: 'service',
 //labels: ['Service']
  // A list of names of data record attributes that contain y-values.
  ykeys: ['no_of_orders'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['orders'],
   parseTime: false

//labels: ['Orders', 'Service']
 // hideHover: false,

});
var data1 = <?php echo json_encode($counters);?>;
var data2 = <?php echo json_encode($names);?>;
var dataArr2 = new Array();
for (var key in data1){
dataArr2.push({ Date: data2[key], value: data1[key] });
}
console.log(dataArr2);


new Morris.Line({


  element: 'myfirstchart2',


data: dataArr2 ,


  // The name of the data record attribute that contains x-values.
  xkey: 'Date',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['No. of orders','responsible'],
  parseTime: false
});


var data3 = <?php echo json_encode($result);?>;
var data4 = <?php echo json_encode($service_names);?>;


var arrdata = new Array();
for (var key in data3){
arrdata.push({ Date: data4[key], value: data3[key] });
}
console.log(arrdata);


new Morris.Line({


  element: 'myfirstchart3',


data: arrdata ,


  // The name of the data record attribute that contains x-values.
  xkey: 'Date',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.

  labels: ['No. of rejected orders','responsible'],

  parseTime: false
});


</script>
@else

<p><h3>{{ trans('tr.There is no services') }} </h3></p>
<b>{{ trans('tr.you can') }} </b><a href="{{ URL::to('service/create') }}">{{ trans('tr.Add Service') }}</a>

@endif
</body>
