<html>
<head>

  <!-- Bootstrap Arabic CSS -->
    <link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



</head>

<body>
  <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/admindash') }}">{{ trans('tr.Admin Dashboard') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/admindash') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/service') }}"><i class="fa fa-table fa-fw"></i>{{ trans('tr.Services') }}</a>
                        </li>
                        <li>
                            <a href="{{ url('/service/create') }}"><i class="fa fa-edit fa-fw"></i>{{ trans('tr.Add Service') }}</a>
                        </li>

                        </li>
                        <li>
                            <a href="{{ url('/responsibleStatics') }}"><i class="fa fa-pencil fa-fw"></i> {{ trans('tr.Statistics Of Responsibles') }}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Services') }} </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
<div>
<div class="row">
 	@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (isset($services))

<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
        <tr class="info">
            <th>{{ trans('tr.Service Name') }}</th>
            <!--th>Service Description</th-->
             <th>{{ trans('tr.action') }}</th>
        </tr>
    </thead>
 <tbody>


@foreach($services as $service)
<tr>
  <td>{{ $service -> name }}</td>
  <!--td>{{ $service -> description }}</td-->
  <td>


                <a class="btn btn-mini btn-success" href="{{ URL::to('service/' . $service->id) }}">{{ trans('tr.Show') }}</a>
                <a class="btn btn-mini btn-info" href="{{ URL::to('service/' . $service->id . '/edit') }}">{{ trans('tr.Edit') }}</a>
		

  {!! Form::open(array('route' => array('service.destroy', $service->id), 'method' => 'delete' , 'style'=>'display: inline;')) !!}
                    <button type="submit" class="btn btn-danger btn-mini" style="display: inline;" >{{ trans('tr.Delete') }}</button>
                {!! Form::close() !!}
             

</td>
{!! Form::Close() !!}
 </tr>
@endforeach
@else

<p><h3>{{ trans('tr.There is no services') }} </h3></p>
<b>You can </b><a href="{{ URL::to('service/create') }}">{{ trans('tr.Add Service') }}</a>

@endif



<div class="col-md-12">
</div>

</div> <!--row -->
</div>
</div>
    <!-- /#wrapper -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.js"></script>
</body>

<script>
$(document).ready(function() {
  $('#example').dataTable( {
   "language": {
    "paginate": {
      "next": "{{ trans('tr.next') }}",
"previous": " {{ trans('tr.previous') }}"
    },
    "search": "{{ trans('tr.search') }}",
"sLengthMenu": "{{ trans('tr.show') }} <select>"+
        '<option value="10">10</option>'+
        '<option value="20">20</option>'+
        '<option value="30">30</option>'+
        '<option value="40">40</option>'+
        '<option value="50">50</option>'+
        '<option value="-1">{{ trans('tr.All') }} </option>'+
        '</select> ',
"sInfo": " {{ trans('tr.Show') }} _START_ {{ trans('tr.to') }} _END_ {{ trans('tr.of') }} _TOTAL_ {{ trans('tr.entries') }} ",
"sInfoEmpty":      "{{ trans('tr.Show') }} 0 to 0 of 0 {{ trans('tr.entries') }} ",
    "sInfoFiltered":   "({{ trans('tr.chosen') }} {{ trans('tr.from') }} _MAX_ {{ trans('tr.entries') }})",
    "zeroRecords":    "{{ trans('tr.No matching records found') }}",
} 

});
} );
</script>
