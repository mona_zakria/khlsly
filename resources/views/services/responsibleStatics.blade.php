<html>
<head>
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />
 <!-- Bootstrap Core CSS -->
    <!--link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"-->

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



</head>

<body>
  <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/admindash') }}">{{ trans('tr.Admin Dashboard') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/admindash') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/service') }}"><i class="fa fa-table fa-fw"></i> {{ trans('tr.Services') }}</a>
                        </li>
                        <li>
                            <a href="{{ url('/service/create') }}"><i class="fa fa-edit fa-fw"></i>{{ trans('tr.Add Service') }}</a>
                        </li>

                        </li>
                        <li>
                            <a href="{{ url('/responsibleStatics') }}"><i class="fa fa-pencil fa-fw"></i> {{ trans('tr.Statistics Of Responsibles') }}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Statistics Of Responsibles') }} </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
<div>
<div class="row">

@if(isset($responsibleStatics))
@foreach($responsibleStatics as $index => $statistic)
<div class="col-md-4">
                    <div class="{{ $colors[$index] }}">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><h4>{{ trans('tr.Responsible Name:') }} {{ $statistic->responsible_name }}</h4></div>
				    <a href="{{  URL::to('/showOrder'.$statistic->order_id) }}" class="huge"><h4>{{ trans('tr.order_id:') }} {{ $statistic->order_id }}</h4></a>
                                    <div><h3>{{ trans('tr.service') }} {{ $statistic->service }}</h3></div>
                                </div>
                            </div>
</div>
</div></div>
@endforeach

@else
<div class="alert alert-info">
<p><h3>{{ trans('tr.There is no orders working on') }} </h3></p>
</div>

@endif
</div> <!--row -->
</div>

</div>
@
    <!-- /#wrapper -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/')}}/arabic/js/bootstrap-arabic.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
</body>
