<html>
<head>

    <!-- Bootstrap Arabic CSS -->
    <link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">



</head>

<body>
  <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/dashboard') }}">{{ trans('tr.Home') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/dashboard') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>

                        <li>     
                             <a  href="{{ URL::to('organizations/') }}"><i class="fa fa-book fa-fw"></i> {{ trans('tr.Manage Organizations') }} </a>
                        </li>
                        <li>
                             <a  href="{{ URL::to('admins/') }}"><i class="fa fa-book fa-fw"></i>{{ trans('tr.Manage Admins') }} </a>
                      </li>
                      <li>
                             <a  href="{{ URL::to('responsibles/') }}"><i class="fa fa-book fa-fw"></i>{{ trans('tr.Manage Responsibles') }} </a>
                    </li>
                        <li>
                              <a  href="{{ URL::to('postals/') }}"><i class="fa fa-book fa-fw"></i>{{ trans('tr.Manage Postals') }} </a>
                        </li>
        


                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.super admin') }}  </h1>


 <div class="col-lg-12">
            <div class="row">
                   <div class="offset3 span6">
                    <div class="col-lg-4 col-md-6" style="text-align: center">
                        <div id ="green" class="panel panel-green" style="text-align: center">
                            <div class="panel-heading" style="text-align: center">
                                <div class="row" style="text-align: center">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>

                                        <div class="huge">{{ $organizations_no }}</div>
                                        <a class="an" color="white" href="{{ URL::to('organizations/') }}">{{ trans('tr.organizations') }}</a>
                                       </div>

                               </div>
                               
                            </div>
                          </div>
                        </div>
                    </div>




<div class="row">
@foreach ($organization_names as $index => $organization_name)
  <div class="offset3 span6">
                    <div class="col-lg-4 col-md-6" style="text-align: center">
                        <div id ="green" class="{{ $colors[$index] }}" style="text-align: center">
                            <div class="panel-heading" style="text-align: center">
                                <div class="row" style="text-align: center">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="small">{{ $organization_name }}</div>
                                        <a class="an" color="white" href="service_super/{{$organization_ids[$index]}}">{{ $services_count[$index] }} : Services  </a>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>

@endforeach



                </div>
                <!-- /.col-lg-12 -->
</div>
<div>



</div> <!--row -->
</div>
</div>
    <!-- /#wrapper -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
</body>
