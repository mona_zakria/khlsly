

<html>
<head>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

	
 <!-- Bootstrap Arabic CSS -->
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

 
<link rel="stylesheet" href="../dist/css/style.css">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  

</head>

<body>
  <div class="wrapper">


      
		

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">


				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">


							<label class="col-md-4 control-label">{{ trans('tr.Email') }} </label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>



							<label class="col-md-4 control-label">{{ trans('tr.Password') }}</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>



							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> {{ trans('tr.Remember Me') }}
									</label>
								</div>
							</div>



							<div class="col-md-6 col-md-offset-4">
								<button type="submit" id="login-button" >{{ trans('tr.Login') }}</button>

								<a class="btn btn-link" href="{{ url('/password/email') }}">{{ trans('tr.Forgot Your Password?') }}</a>
 <a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a>
							</div>

					</form>
				</div>
			</div>
		</div>
	</div>

	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
</body>
