<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    
        <link rel="stylesheet" href="../dist/css/style.css">

       <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    
  </head>

  <body>

    <div class="wrapper">
	<div class="container">

				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">

							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
		
		<form class="form" role="form" method="POST" action="{{ url('/auth/login') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="text" name="email" placeholder="{{ trans('tr.Email') }}" value="{{ old('email') }}">
			<input type="password" name="password" placeholder="{{ trans('tr.Password') }}">
			<button type="submit" id="login-button">{{ trans('tr.Login') }}</button></br>
			<label style="color:white;">{{ trans('tr.Remember Me') }}</label>
			<input type="checkbox" name="remember"> 

		</form>
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>

    
    
    
  </body>
</html>
