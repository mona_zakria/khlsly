<!DOCTYPE html>
<html>
<head>

@extends("master")
@section("content")
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('orders') }}">View All Admins</a></li>
        <li><a href="{{ URL::to('orders/index') }}">Create an Admin</a>
    </ul>
</nav>
<h1>ِAdd Order </h1>

@foreach($atrributes as $attribute)
{{-- $attribute -> service_id --}}

{!! Form::open(array('url' => "services/$attribute->service_id/orders/new", 'files' => true)) !!}
 <div class="form-group">
{!! Form::label($attribute -> name) !!}
{!! Form::input( $attribute->type->name , "$attribute->name"  ) !!}
 </div>
@endforeach
<div class="form-group">
{!! Form::label("Address") !!}
{!! Form::input('text' ,"address") !!}
 </div>
{!! Form::submit('Save!', array('class' => 'btn btn-primary')) !!}
{!! Form::Close() !!}
@stop
</div>
</body>
</html>

