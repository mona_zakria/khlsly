<html>
	<head>


<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
    <style>
      body {
        font-family: 'Tangerine', serif;
        font-size: 18px;
      }
    </style>



<script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/')}}/arabic/js/bootstrap-arabic.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->




<!-- Bootstrap Arabic CSS -->
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<script type="text/javascript" src="jquery.simple-dtpicker.js"></script>
<link type="text/css" href="jquery.simple-dtpicker.css" rel="stylesheet" />



<script>
  $(function() {
    $( "#action_date" ).datepicker();
  });
  </script>

<link rel="stylesheet" href="easy/css/example.css" />
  <link rel="stylesheet" href="easy/css/pygments.css" />
  <link rel="stylesheet" href="easy/css/easyzoom.css" />

 
	</head>
	<body>




 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">{{ trans('tr.Home') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/orders/3') }}"><i class="fa fa-table fa-fw"></i>{{ trans('tr.Orders') }}</a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>




<div id="page-wrapper">
          

<div class="row">

<div class="col-md-12 ">
   <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"  >{{ trans('tr.Order of') }} {{ $ser_name }} </h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
@foreach ($sent_files as $index => $upload )

<!-- <a data-lightbox="{{ $upload }}"  class="image-thumb" href="{{ $upload }}">
 -->
<div class="easyzoom easyzoom--overlay">
        <a href="{{ $upload }}">
          <img src="{{ $upload }}" alt="" width="500" height="500" />
        </a>
      </div>



<br>
@endforeach




 
  <u><b>{{ trans('tr.Add action:') }}</b></u>


<form action="f_status"  >



            <div class="btn-group" >


  <label >  <input id = "acc" type="radio" name="a" value="3"  checked > {{ trans('tr.accepted') }}    </label> &nbsp;



 <label > <input id = "rej" type="radio" name="a" value="2"  > {{ trans('tr.rejected') }} </label>

</div>

<input id = "rej" type="hidden" name="act" >


<input class="ckeditor" id = "comment" type="hidden" name="comment" placeholder="{{ trans('tr.Enter your comment here ..') }}" onfocus="if (this.value == 'Enter your comment here .. ') {this.value = '';}">
   
   <!-- <input class="form-control" class = "date" id = "action_date" type="date" name="action_date"  >
 --> 
 <input type="text" class = "date" id="action_date" name="action_date" placeholder= "{{ trans('tr.Enter date here ..') }}" >

<input id = "order_id" type="hidden" name="order_id" value="{{ $order_id }}"  >


<input type="submit" value="{{ trans('tr.Submit') }}" class="btn btn-primary" >

</form> 


</div> <!--row -->
</div>
</div>
    <!-- /#wrapper -->


  

<script >

var flag;
 act = document.getElementsByName("act")[0];
   comment = document.getElementById("comment");
   action_date = document.getElementById("action_date");
document.getElementById("action_date").required = true;
 act.value=2;
console.log(flag);
  acc = document.getElementById("acc");
 rej = document.getElementById("rej");
console.log(act.value);
if(rej.checked)
{
	comment.type="textbox";
action_date.type="hidden";
document.getElementById("action_date").required = false;
document.getElementById("comment").required = true;
}


acc.onclick = function(){
act.value=2;
flag=1;
comment.type="hidden";
action_date.type="text";
document.getElementById("comment").required = false;
document.getElementById("action_date").required = true;
//	alert("acc");
}

rej.onclick = function(){
	flag=0;

act.value=3;
comment.type="textbox";
action_date.type="hidden";
document.getElementById("action_date").required = false;
document.getElementById("comment").required = true;

//	alert(act.value);
}

</script>


  <script src="easy/dist/easyzoom.js"></script>
  <script>
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Get an instance API
    var api = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    // Setup thumbnails example
    $(".thumbnails").on("click", "a", function(e) {
      var $this = $(this);

      e.preventDefault();

      // Use EasyZoom's `swap` method
      api.swap($this.data("standard"), $this.attr("href"));
    });
  </script>


	</body>    
</html>
