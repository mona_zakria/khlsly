<html>
<head>

<style>


body {
   /* background-image: url("bch.jpg");*/
}

table, th, td {
    border: 2px solid black;
}
nav {
    position: absolute;
    left: 0;
    background-color: #F0FFF0;
    float: left;
    width: 100%;
    margin: 0 auto;
    max-width: 1950px;

}

/*#green 
{
background-color: green;

}*/
</style>




 <!-- Bootstrap Arabic CSS -->
<link rel='stylesheet' href="{{url('/')}}/arabic/css/bootstrap-arabic.css" type='text/css' />

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="../dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
</head>

<body>
  <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">{{ trans('tr.Home') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
              
               @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">{{ trans('tr.Login') }}</a></li>
						<li><a href="{{ url('/auth/register') }}">{{ trans('tr.Register') }}</a></li>
		@else
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>{{ Auth::user()->name }}</a>
                        </li>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> {{ trans('tr.Logout') }}</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            @endif
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ url('/') }}"><i class="fa  fa-home fa-fw"></i>{{ trans('tr.Home') }} </a>
                        </li>
                       
                        <li>
                            <a href="{{ url('/orders/3') }}"><i class="fa fa-table fa-fw"></i> {{ trans('tr.Orders') }}</a>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{ trans('tr.Responsible Overview') }}</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>

<div class="row">

                   <div class="offset3 span6">
                    <div class="col-lg-3 col-md-6" style="text-align: center">
                        <div id ="green" class="panel panel-green" style="text-align: center">
                            <div class="panel-heading" style="text-align: center">
                                <div class="row" style="text-align: center">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $new_orders }}</div>
                                        <a class="an" color="white" href="/orders/1">{{ trans('tr.New orders') }}</a>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                               
                            </a>
                        </div>
                    </div>
                </div>
    

     <div class="col-lg-3 col-md-6" style="text-align: center">
                        <div class="panel panel-red" style="text-align: center">
                            <div class="panel-heading" style="text-align: center">
                                <div class="row" style="text-align: center">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $updated_orders }}</div>
                                              <a class="an" color="white" href="/orders/2">{{ trans('tr.Updated orders') }}</a>

                                    </div>
                                </div>
                            </div>
                        
                                
                            </a>
                        </div>
                    </div>



<h1>    <strong>{{ trans('tr.Current') }}<font color="green"> <br>{{ $organization_name }}</font> </strong> </h1>
        {{ trans('tr.To view orders') }} <a href="/orders/3">{{ trans('tr.click here !') }}</a>


<div id="myfirstchart" style="height: 200px;"></div>








@if ( $services )
<h2> {{ trans('tr.under processing orders')}} </h2>
<div class="col-md-12">
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
  <thead>
        <tr class="info">
                 <th>{{ trans('tr.name of service') }}</th>
            <!--th>Service Description</th-->
                 <th> {{ trans('tr.name of requester') }}</th>
         <th>{{ trans('tr.date of order') }} </th>
         <th>{{ trans('tr.status of order') }}</th>
        </tr>
    </thead>
 <tbody>
@foreach ($services as $index => $service )

<tr>
<td>  {!! Html::linkAction('ordersController@show', $service->name , $orders_table[$index]->id ) !!} </td>


<td>{{ $user_names[$index] }} </td>
<td> {{ $orders_table[$index]->date }}</td>

@if ( $status[$index]  == "8")

<td> {{ trans('tr.Locked') }}</td>

<!-- {{ $status[$index] }} = "New";
 -->@endif

</tr>
<br>
@endforeach
</tbody>
</table>
</div>

@endif












 <!-- /#wrapper -->
  <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
<script>
var data1 = <?php echo json_encode($month_orders );?>;
var data2 = <?php echo json_encode($month_arr );?>;
var dataArr = new Array();
for (var key in data1){
dataArr.push({ Date: data2[key], value: data1[key] });
}
console.log(dataArr);


new Morris.Line({


  element: 'myfirstchart',


data: dataArr ,


  // The name of the data record attribute that contains x-values.
  xkey: 'Date',
  // A list of names of data record attributes that contain y-values.
  ykeys: ['value'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['No. of orders','Month'],
  parseTime: false
});



</script>
</body>
