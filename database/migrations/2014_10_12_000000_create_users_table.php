<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->integer('type');
			$table->softDeletes();
			$table->rememberToken();
			$table->timestamps();
		});

	    DB::table('users')->insert(
	        array(
	            'name' => 'asmaa',
	            'email' => 'asmaakhaled01@gmail.com',
	            'password' => '$2y$10$T6tBIXZYkCZwQbnqFSSKVOe3xjhj0QE7tt8iGS7RPLm6l1P.EiuXq',
	            'type' => '1'
	        )
	    );



	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
