<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('date');
			$table->integer('requester_id')->unsigned();
			$table->foreign('requester_id')->references('id')->on('requesters');

			$table->integer('service_id')->unsigned();
			$table->foreign('service_id')->references('id')->on('services');


			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
				Schema::drop('orders');
	}

}
