<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeTable extends Migration {

	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attributes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->unique();
		    $table->boolean('mandatory');

			$table->integer('service_id')->unsigned();
			$table->foreign('service_id')->references('id')->on('services');
			
            $table->integer('type_id')->unsigned();
			$table->foreign('type_id')->references('id')->on('types');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attributes');
	}

}
